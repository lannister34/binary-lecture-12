import CartParser from './CartParser';

let parser, cart, wrongCart;

beforeEach(() => {
    parser = new CartParser();
    cart = parser.readFile('samples/cart.csv');
});

describe('CartParser - unit tests', () => {
    // Add your unit tests here.
    it('correct parse line', () => {
        const result = parser.parseLine('Scelerisque lacinia,18.90,1');
        expect(result).toMatchObject({
            "id": expect.any(String),
            "name": "Scelerisque lacinia",
            "price": 18.9,
            "quantity": 1
        });
    });

    it('correct validation', () => {
        const result = parser.validate(cart);
        expect(result).toEqual([]);
    });

    it ('header validation error', () => {
        const wrongCart = cart.replace('Product name', 'Wrong Header');
        const result = parser.validate(wrongCart);
        expect(result).not.toEqual([]);
    });

    it ('format validation error', () => {
        const wrongCart = cart.replace(',', ' ');
        const result = parser.validate(wrongCart);
        expect(result).not.toEqual([]);
    });

    it ('empty string cell error', () => {
        const wrongCart = cart.replace('Mollis consequat', '');
        const result = parser.validate(wrongCart);
        expect(result).not.toEqual([]);
    });

    it ('not a number cell error', () => {
        const wrongCart = cart.replace('9.00', NaN);
        const result = parser.validate(wrongCart);
        expect(result).not.toEqual([]);
    });
});


describe('CartParser - integration test', () => {
    // Add your integration test here.
    it('Correct system work', () => {
        const result = parser.parse('samples/cart.csv');
        expect(result).toMatchObject({
            items: expect.any(Array),
            total: expect.any(Number)
        });
        result.items.forEach((item) => {
            expect(item).toMatchObject({
                "id": expect.any(String),
                "name": expect.any(String),
                "price": expect.any(Number),
                "quantity": expect.any(Number)
            })
        });
    });
});